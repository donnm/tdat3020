#include <stdio.h>

// gcc -o rop rop.c -no-pie -fno-stack-protector

void func()
{
    printf("This function never called\n");
}

int main()
{
    char buff[32];
    gets(buff);
    printf(buff);
}
