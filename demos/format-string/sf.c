#include <stdio.h>
int main()          
{                   
    int before = 1337;
    char buffer[128];
    int after = 42;
    printf("before=%d at 0x%x\n", before, &before);
    printf("after=%d at 0x%x\n", after, &after);
    fgets(buffer,sizeof(buffer),stdin); 
    printf(buffer);   
    printf("before=%d at 0x%x\n", before, &before);
    printf("after=%d at 0x%x\n", after, &after);
}                   

